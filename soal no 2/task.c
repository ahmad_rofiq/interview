

//Pseudocode
init(porta)
init(adc) 
init(timer)
loop forever:
	read adc value to 
	compare adc value with temp setpoint
	if current temp > setpoint then start actuator
	if no then stop actuator
	wait timer for overflow in 1ms * 1000 (1s)
	do led toggle




//code in c
#define PORTA 		0x1000
#define ADC 		0x2000
#define TIMER		0x3000
#define	LED			0
#define	ACTUATOR	1
#define	ADC0		0
#define	TIMEROF		0										  // timer overflow flag bit 0
#define	TIMESTART	1										  // timer start bit 1
#define SETPOINT 	621										  // 1 deg C / 100 mV on 10 bit ADC (3.3V ref)

//pointer variable to the port, timer, and adc
unsigned int* const port 	  = (unsigned int *) PORTA;		  // set port address to 0x1000
unsigned int* const adcPort   = (unsigned int *) ADC;		  // set adc address to 0x2000
unsigned int* const timer     = (unsigned int *) TIMER;	  	  // set timer address to 0x2000


void init (void)
{
	//set 32 bit timer with 1ms
	//set ADC to 10 bit 
}

void main(void)
{
	init();
	static int time = 0;		// set timer value to 1000 ms.
	int adcvalue;
	
	// loop endlessly
	while (true)
	{
		// Start an ADC conversion by setting ADC bit 0
		*adcPort |= 1 << ADC0;						// statt ADC0
		
		// Wait until the ADC bit has been cleared (ADC process finish)
		while(*adcPort & (1 << ADC0)); 
		
		adcvalue = *adcPort;
		(adcvalue & 0xffc0) >> 6					// shift upper 10 bit adc
		if(adcvalue >= SETPOINT){					// compare adc value with set point 20 degree.
			*port |= 1 << ACTUATOR;					// actuator ON
		}else{
			*port &= ~(1 << ACTUATOR);				// actuator OFF
		}
		
		
		while(time < 1000){ 
			*timer |= 1 << TIMESTART;				// start timer for 1 ms
			while((*timer & (1<<TIMEROF)) == 0);	// wait till the timer overflow flag is SET (1ms)
			TIFR |= (1<<TIMEROF) ; 					// clear timer1 overflow flag
			time++;		
		}
		
		time = 0;
		PORTD ^= (1<< LED);							// led togle
		
	}
}
