#include <iostream>

using namespace std;

int main(int argc, char const *argv[])
{
    int num = 0;
    int array[] = { 2, 4, 6, 2, 5, 2, 4, 7, 8, 8, 20, 21, 15, 11, 6, 5, 4, 20, 21 , 15, -2, 3, 0, 25, 1, -100000};

    int size = sizeof(array) / sizeof(array[0]); 
    cout << "size: " << size << endl;

    //Sorting
    printf("\n\n");
    int i, j, temp;
    for (i = 0; i < size; i++)
    {
        for (j = 0; j < (size - i - 1); j++)
        {
            if (array[j] > array[j + 1])
            {
                temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }

    printf("=== Sorted array ===\n");
    for (i = 0; i < size; i++)
    {
        printf("%d ", array[i]);
    }

    //Remove duplicate
    //printf("\n\n=============\n");
    int arrayBaru[size] = {};
    int skipVal = 0;
    int arrayBaruPointer = 0;
    int sizeArrayBaru = 0;

    for (int i = 0; i < size; i++) { 
        //printf("i: %d - %d \n", i, array[i]);
        for (int j = i + 1; j < size; j++) {
            //cout << "j: " << j << ", array[i]: " << array[i] << " ,array[j]: " << array[j] << endl;
            if (array[i] != array[j]) {
                arrayBaru[arrayBaruPointer] = array[i];
                arrayBaruPointer++;
                sizeArrayBaru++;
                //printf("\n");
                break;
            }else{
                skipVal++;
            }
        }

        /* Last element */
        if(i == (size-1)){
            //printf("\nlast element check\n");
            int len = sizeof(arrayBaru) / sizeof(arrayBaru[0]); 
            int check = 0;
            for (int x = 0; x < sizeArrayBaru; x++) {
                //printf("\nlast element check array[%d]: %d",x,arrayBaru[x]);
                if(array[i] ==  array[x]){
                    break;
                }else{
                    //printf("\nlast element added");
                    check++;
                }
            }

            //printf("\ncheck : %d, sizeArrayBaru: %d", check, sizeArrayBaru);

            if(check == sizeArrayBaru){
                //printf("\ncheck sama");
                arrayBaru[sizeArrayBaru] = array[i];
                sizeArrayBaru++;
            }
        }

        i=i+skipVal;
        skipVal = 0;
    }

    printf("\n\n====== Array Baru =======\n");
    for (i = 0; i < sizeArrayBaru; i++)
    {
        printf("%d ", arrayBaru[i]);
    }

    printf("\n\nNumber of distict value: %d\n",sizeArrayBaru);
    //int sizeBaru = sizeof(arrayBaru) / sizeof(arrayBaru[0]); 
    

    /* SHIFT + CTRL + P to Task: Run Build Task or SHIFT + CTRL + B */
    return 0;
}
